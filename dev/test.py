import time
WORK_TIME = 60 * 3
from pwn import *
all_req = 0
def MakeRequest(J,all_req):
    i = 30
    while(i >= 0):
        #r = remote('178.154.216.245', 18661)
        r = remote('127.0.0.1', 18661)
        r.recvline()
        r.sendline("test")
        res = r.recvline()
        if ("Bad asnw" in str(res)):
            J += 1
        #r.close()
        all_req += 1
        time.sleep(1. / 30.)
        i -= 1

J = 0
if (__name__ == "__main__"):
    cur_time = time.time()
    while (time.time() - cur_time <= WORK_TIME):
        MakeRequest(J,all_req)
    
    print(f"{J}/{all_req}")
