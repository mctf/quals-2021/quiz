import pandas as pd
import base64
import asyncio
import socket
flag = ''
with open("flag.txt", 'r') as f:
    flag = f.read()
async def task(client):

    questions_answer = pd.read_csv("quest_answ.csv")

    i = 1
    loop = asyncio.get_event_loop()
    for index, row in questions_answer.iterrows():
        data = ''
        with open('images/' + str(i) + '.png', 'rb') as image:
            bin_data = image.read()
            data = base64.b64encode(bin_data)
        try:
            await loop.sock_sendall(client, data + b'\n')
        except BrokenPipeError:
            client.close()
            return
        try:
            answ = (await loop.sock_recv(client, 255)).decode('utf8')
        except BrokenPipeError:
            client.close()
            return
        if (answ[:-1] != row['ANSW'] + '.'):
            try:
                await loop.sock_sendall(client, "Bad asnw!\n".encode('utf8'))
                break
            except BrokenPipeError:
                client.close()
                return

        i += 1
    else:
        await loop.sock_sendall(client, flag.encode('utf8') + b'\n')
    client.close()

async def run_server():
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.bind(('0.0.0.0', 18668))
    server.listen(50)
    server.setblocking(False)

    loop = asyncio.get_event_loop()

    while True:
        client, _ = await loop.sock_accept(server)
        loop.create_task(task(client))

asyncio.run(run_server())


