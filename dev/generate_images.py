from PIL import Image, ImageDraw, ImageFont
import pandas as pd
def encode(text, message, dest):
    img = Image.new('RGB', (3000, 150), color=(73, 109, 137))
    fnt = ImageFont.truetype('/Library/Fonts/Arial.ttf', 30)
    d = ImageDraw.Draw(img)
    d.text((1, 1), "M*CTF quiz", font=fnt, fill=(255, 255, 0))
    d.text((50, 50), text, font=fnt, fill=(255, 255, 0))
    data = ''.join([format(ord(i), "08b") for i in message])
    i=0
    width, height = img.size
    for x in range(0, width):
        for y in range(0, height):
            pixel = list(img.getpixel((x, y)))
            for n in range(0,3):
                if(i < len(data)):
                    pixel[n] = pixel[n] & ~1 | int(data[i])
                    i+=1
            img.putpixel((x,y), tuple(pixel))
    img.save(dest, "PNG")

questions_answer = pd.read_csv("quest_answ.csv")

i = 1
for  index, row in questions_answer.iterrows():
    encode(row['QUESTIONS'],row['ANSW'] + '.', 'images/' + str(i) + '.png')
    i+=1

