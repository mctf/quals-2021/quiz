FROM debian:11.0

RUN apt-get update && apt-get install -y python3 python3-pip
ADD dev/requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

ADD dev/server.py /chall/server.py
ADD dev/quest_answ.csv /chall/quest_answ.csv
ADD dev/flag.txt /chall/flag.txt
ADD dev/images /chall/images

EXPOSE 18668
USER 1001
WORKDIR /chall
ENTRYPOINT ["python3", "server.py"]

