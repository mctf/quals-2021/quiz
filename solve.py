from pwn import *
import pandas as pd
import base64
from PIL import Image
questions_answer = pd.read_csv("quest_answ.csv")

def decode_binary_string(s):
    return ''.join(chr(int(s[i*8:i*8+8],2)) for i in range(len(s)//8))

def decode(data):
    #print(data)
    #data = data[2:-1]
    decode_base = base64.b64decode(data)
    with open("test.png",'wb') as f:
        f.write(decode_base)

    img = Image.open("test.png")
    width, height = img.size
    bin_data = ''
    max_size_string = 100 * 8
    for x in range(0, width):
        for y in range(0, height):
            pixel = list(img.getpixel((x, y)))
            for n in range(0,3):
                if( max_size_string >= 0):
                    bin_data += str(pixel[n] & 1)
                    max_size_string -= 1

    answ = decode_binary_string(bin_data)
    answ = answ[:answ.find(".") + 1]
    return answ
#r = remote('178.154.216.245', 18668)
r = remote('127.0.0.1', 18668)
while(True):
    data = r.recvline()
    if ("mctf" in str(data)):
        print(data)
    answ = decode(data)
    print(answ)

    r.sendline(answ.encode('utf-8'))

data = r.recvline()
print(data)
